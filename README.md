Webs2-Webshop
=============

This was a school project for Avans Hogeschool 's-Hertogenbosch in the Netherlands. It was made from the Bootshop Bootstrap Template.

## Screenshot
![My image](https://raw.githubusercontent.com/darthsitthiander/webs2-webshop/master/public/images/screenshots/IgfeDnM.png)

## Installation

- Copy the files to a server
- Give the folder app/storage 777 permissions
- Edit <our database credentials in the file app/config/database.php
- Import the database database/webs2-webshop.sql to your mysql databases OR migrate and seed from the files in app/database
- Composer update
