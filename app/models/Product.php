<?php

class Product extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	protected $fillable = array('fk_category', 'name', 'description', 'price_eur', 'discount', 'tax', 'long_description', 'featured', 'featured_sidebar', 'stock');

	protected $key = "id";

	public static $rules = array(
		'fk_category'=>'required|integer',
		'name'=>'required|min:2',
		'description'=>'required|min:20',
		'long_description'=>'required|min:40',
		'price_eur'=>'required|numeric',
		'discount'=>'required|numeric',
		'tax'=>'required|numeric',
		'featured'=>'integer',
		'featured_sidebar'=>'integer',
		'stock'=>'integer',
	);

	public function category() {
		return $this->belongsTo('Category');
	}
}