<?php

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	protected $fillable = array('name', 'parent_category', 'description');

	protected $key = "id";

	public static $rules = array('name'=>'required|min:3');

	public function products() {
		return $this->hasMany('Product', 'fk_category');
	}
}