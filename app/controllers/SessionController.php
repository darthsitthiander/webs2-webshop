<?php

class SessionController extends \BaseController {

	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postLogin()
	{
		$input = Input::all();

		$rules = array(
	        'email'    => array('required', 'email'),
	        'password' => array('required', 'min:8')
	    );

	    $validation = Validator::make($input, $rules);

	    if ($validation->fails())
	    {
	        // Validation has failed.
	       	return Redirect::back()->with(array('message' => 'Something went wrong with the validation.', 'alert-class'=>'alert-warning'))->withInput()->withErrors($validation);
	    }

	    // Validation has succeeded. Create new user.
	    
	    $remember = (isset($input['remember_me'])) ? true : false;

		$attempt = Auth::attempt([
			'email' => $input['email'],
			'password' => $input['password']
		], $remember);

		// Login successful
		if($attempt)
		{
			return Redirect::intended('/')->with(array('message' => 'You have been logged in.', 'alert-class'=>'alert-success'));
		} else {
			return Redirect::back()->with(array('message' => 'Invalid credentials.', 'alert-class'=>'alert-danger'))->withInput();
			
		}		
	}

	public function postRegister()
	{
		$input = Input::all();

		$rules = array(
			'fname'=>'required|min:2|alpha',
			'lname'=>'required|min:2|alpha',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|between:8,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:8,12',
			'dDay'=>'required|integer',
			'dMonth'=>'required|integer',
			'dYear'=>'required|integer',
			'address'=>'required|alpha_spaces',
			'city'=>'required|alpha_dash',
			'zip_code'=>'required|alpha_num',
		);

		$messages = array(
		    'fname.required' => 'We need to know your first name.',
		    'lname.required' => 'We need to know your last name.',
		    'email.required' => 'We need to know your e-mail address.',
		    'password.required' => 'You must enter a password.',
		    'password_confirmation.required' => 'You must confirm your password!',
		    'dDay.required' 	=> 'The \'Day\' field of your date of birth credentials is required.',
		    'dMonth.required' 	=> 'The \'Month\' field of your date of birth credentials is required.',
		    'dYear.required' 	=> 'The \'Year\' field of your date of birth credentials is required.',
		    'email.required' 	=> 'We need to know your e-mail address.',
		    'address.required'			=> 'We need to know your address.',
			'city.required'				=> 'We need to know the city you live in.',
			'zip_code.required'			=> 'We need to know your ZIP code.'
		);

		$v = Validator::make($input, $rules, $messages);

		if($v->passes())
		{
			$password = $input['password'];
			$password = Hash::make($password);

			$user = new User();
			$user->email = $input['email'];
			$user->first_name = $input['fname'];
			$user->last_name = $input['lname'];
			$user->email = $input['email'];
			$user->password = $password;
			$user->date_of_birth = $input['dYear'].'-'.$input['dMonth'].'-'.$input['dDay'];
			$user->address = $input['address'];
			$user->city = $input['city'];
			$user->zip_code = $input['zip_code'];

			$user->save();

			return Redirect::intended('/')->with(array('message' => 'Your account has been made successfully.', 'alert-class'=>'alert-success'));
		} else {

			return Redirect::back()->withErrors($v)->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();

		// Empty Cart
		Cart::destroy();

		return Redirect::home()->with(array('message' => 'You have been logged out.', 'alert-class'=>'alert-success'));
	}

}