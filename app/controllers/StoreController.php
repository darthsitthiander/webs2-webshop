<?php

class StoreController extends BaseController {

	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('auth', array('only'=>array('getCart', 'getRemoveitem')));
	}

	public function getIndex() {
		return View::make('pages.store.index')
			->with('products', Product::take(4)->orderBy('created_at', 'DESC')->get());
	}

	public function getView($id) {
		return View::make('pages.store.view')->with('product', Product::find($id));
	}

	public function getCategory($cat_id) {
		return View::make('pages.store.category')
			->with('products', Product::where('fk_category', '=', $cat_id)->paginate(6))
			->with('category', Category::find($cat_id));
	}

	public function getCart()
	{
		return View::make('pages.store.cart');
	}


	public function postAddtocart() {
		$product = Product::find(Input::get('id'));
		$quantity = Input::get('quantity');

		Cart::insert(array(
			'id'=>$product->id,
			'fk_category'=>$product->fk_category,
			'name'=>$product->name,
			'description'=>$product->description,
			'price'=>$product->price_eur,
			'tax'=>$product->tax,
			'long_description'=>$product->long_description,
			'featured'=>$product->featured,
			'featured_sidebar'=>$product->featured_sidebar,
			'stock'=>$product->stock,

			'quantity'=>$quantity,
		));
		
		return Redirect::to('store/cart');
	}

	public function getIncrementitemincart($identifier)
	{
		$item = Cart::item($identifier);
		$item->quantity += 1;
		return Redirect::to('store/cart');
	}	

	public function getDecrementitemincart($identifier)
	{
		$item = Cart::item($identifier);
		if ($item->quantity > 1)
		{
			$item->quantity -= 1;
		}
		return Redirect::to('store/cart');
	}

	public function getRemoveitem($identifier) {
		$item = Cart::item($identifier);
		$item->remove();
		return Redirect::to('store/cart');
	}

	public function getDestroycart() {
		Cart::destroy();
		return Redirect::to('/')->with(array('message' => 'Your order has been received!</strong> Thanks for your time.', 'alert-class' => 'alert alert-success'));
	}

	public function getCheckout()
	{
		return View::make('pages.store.checkout');
	}
}