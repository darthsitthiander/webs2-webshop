<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/**
     * Show the home page
     */
	public function showHome()
	{
		return View::make('pages.store.index');
	}

	/**
     * Show the home page
     */
	public function showSearch()
	{
		return View::make('pages.store.search');
	}

    /**
     * Show the profile for the given user
     */
    public function showProfile($id)
    {
        $user = User::find($id);

        return View::make('pages.user.profile', array('user' => $user));
    }

	/**
     * Show the register form
     */
    public function showRegister()
    {
        if(!Auth::check())
		{
        	return View::make('pages.sessions.register');
		} else {
			return Redirect::to('/')->with(array('message' => 'You are already registered.', 'alert-class' => 'alert alert-warning'));
		}
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function showLogin()
	{
		if(!Auth::check())
		{
			return View::make('pages.sessions.login');
		} else {
			return Redirect::to('/')->with(array('message' => 'You are already logged in.', 'alert-class' => 'alert alert-warning'));
		}
	}

	public function showProductDetails()
	{
		return View::make('pages.store.product.details');
	}

	public function showCategoriesAll()
	{
		return View::make('pages.store.categories.all');
	}

	public function showCategoriesOverview()
	{
		return View::make('pages.store.categories.overview');
	}

	public function showCategoriesSubcategories()
	{
		return View::make('pages.store.categories.subcategories');
	}

	public function showContact() {
		return View::make('pages.store.contact');
	}	

	public function showTac() {
		return View::make('pages.store.tac');
	}

	public function showFaq() {
		return View::make('pages.store.faq');
	}

	public function getSearch() {
		return View::make('pages.store.search');
	}
}
