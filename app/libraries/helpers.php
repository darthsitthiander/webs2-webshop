<?php
class Helpers {

	public static function getImagePaths($id) {
		$paths = array();

		$basePath = "images/products/";
		$basePathThumbnails = "images/products/thumbnail/";

		$i = 0;
		foreach (glob("$basePath$id.*") as $filename) {
		    $paths[$i]['normal'] = $filename;
		}
		foreach (glob("$basePathThumbnails$id.*") as $filename) {
		    $paths[$i]['thumbnail'] = $filename;
		}

		return $paths;
	}

	public static function generateMenu()
	{
		$this->items['Menu'] = Menu::all();
	}
}