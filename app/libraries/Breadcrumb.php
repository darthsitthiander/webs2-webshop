<?php

namespace BreadcrumbGateway;

use URL;

class Breadcrumb {

	private $items;
	private $page;

	public function __construct()
	{
		$this->items = [];
	}

	public function getItems() {
		$this->makeHome();
		array_push($this->items, $this->page);
		return $this->items;
	}

	/*public function removeAll() {
		$this->items = array();
	}*/

	public function add($item) {
		$this->items[] = $item;
	}

	public function addItem($name, $link, $enabled = true) {
		$this->add(array(
			"name" 		=> $name,
			"link" 		=> $link,
			"enabled" 	=> $enabled
		));
	}

	public function setPage($title) {
		$this->page = array(
			"name" 		=> $title,
			"link" 		=> '',
			"enabled" 	=> false
		);
	}

	public function makeHome(){
		array_unshift($this->items, array(
			"name" 		=> "Home",
			"link" 		=> URL::to('/'),
			"enabled" 	=> true
		));
	}
}