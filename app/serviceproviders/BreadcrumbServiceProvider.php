<?php

use Illuminate\Support\ServiceProvider;

class BreadcrumbServiceProvider extends ServiceProvider {
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('breadcrumb', function(){
            return new \BreadcrumbGateway\Breadcrumb;
        });
    }
}