<!-- FEATURED PRODUCTS -->
			<div class="well well-small">
				<h4>
					@lang('messages.featuredProducts')
					<small class="pull-right">{{ count($products) }} featured products</small>
				</h4>
				<div class="row-fluid">
					<div id="featured" class="carousel slide">
						<div class="carousel-inner">
<?php
$newline = false;
foreach ($products as $key=>$product) {
    if ($key+1 == 1) {
?>
							<div class="item active">
				  				<ul class="thumbnails">
<?php
    } elseif ($newline == true) {
    	$newline = false;
?>
							<div class="item">
				  				<ul class="thumbnails">
<?php
	}
?>
									<li class="span3">
							    		<div class="thumbnail">
							   				<i class="tag"></i>
											<a href="<?= URL::to('product/details/' . $product->id) ?>">
			@if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
									            <a class="gallery" href="<?= (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) ?>" title="<?=$product->name?>">
													<img src="<?= (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) ?>" alt="<?=$product->name?>"/>
									            </a>
			@else
												<div style="padding-top: 20px;">
												No Image available.
												</div>
			@endif  
											</a>
											<div class="caption">
												<h5><?=$product->name?></h5>
												<h4><a class="btn" href="<?= URL::to('product/details/' . $product->id) ?>">VIEW</a> <span class="pull-right">€<?=$product->price_eur?></span></h4>
											</div>
						  				</div>
									</li>
<?php
	if (($key+1) % 4 == 0 || ($key+1) == count($products)) {
?>
								</ul>
							</div>
<?php
		if (($key+1) % 4 == 0)
		{
			$newline = true;
		}
	}
?>
<?php
}
?>
				  		</div>
						<a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
						<a class="right carousel-control" href="#featured" data-slide="next">›</a>
					</div>
				</div>
			</div>