	<div class="tab-pane  active" id="blockView">
		<ul class="thumbnails">
		@foreach (array_chunk($products->getCollection()->all(), 3) as $row)
			<div class="row3">
				@foreach($row as $product)
				<li class="span3">
				  {{ Form::open(array('action' => 'StoreController@postAddtocart', 'method'=>'post')) }}
				  	<div class="thumbnail">
						<div class="gallery span3" style="margin: 0; text-align: center; vertical-align: middle; line-height: 60px;">
				            @if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
				            <a class="gallery" href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="{{ $product->name }}">
								<img src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" alt="{{ $product->name }}"/>
				            </a>
				            @else
				            <div style="padding: 20px;">
							No Image available.
							</div>
				            @endif
	  
						</div>
						<div class="caption">
						  	<h5>{{ $product['name'] }}</h5>
						  	<p>{{ $product['description'] }}</p>
						   	<h4 style="text-align:center">
						   		<a class="btn" href="{{ URL::to('product/details/' . $product['id']) }}">
						   			<i class="icon-zoom-in"></i>
						   		</a>
						   		<button type="submit" class="btn">Add to <i class="icon-shopping-cart"></i></button>
						   		<a class="btn btn-primary">€{{ $product['price_eur'] }}</a>
						   	</h4>
						</div>
				    </div>
				    {{ Form::hidden('quantity', 1) }}
			        {{ Form::hidden('id', $product->id) }}
					{{ Form::token() . Form::close() }}
				</li>
				@endforeach
			</div>
		@endforeach
		</ul>
		<hr class="soft"/>
	</div>