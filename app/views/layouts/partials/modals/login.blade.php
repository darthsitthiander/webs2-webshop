{{ Form::open(array('url' => 'sessions/login', 'method'=>'POST', 'class' => 'form-horizontal loginFrm', 'data-target' => '#login', 'data-async' => 'true')) }}

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3>Login</h3>
</div>

@include('layouts.partials.errors.validation')

<div class="modal-body">
  <div class="control-group">								
	{{ Form::text('email', '', array('placeholder' => 'Email', 'id' => 'inputEmail')) }}
  </div>
  <div class="control-group">
	{{ Form::password('password', array('placeholder' => 'Password', 'id' => 'inputPassword', 'value' => Input::old('password'))) }}
  </div>
  <div class="control-group">
	<label class="checkbox">
	<input type="checkbox" name="remember_me"> Remember me
	</label>
  </div>
  <div class="modal-footer">
  	<div class="btn-toolbar">
		{{ Form::submit('Login', array('class' => 'btn btn-success pull-left')) }}
		{{ Form::button('Close', array('class' => 'btn pull-right', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
		{{ HTML::link(URL::to('sessions/register'), 'Register', array('class' => 'btn btn-info pull-right', 'style' => 'line-height: 20px')) }}
  	</div>
  </div>
</div>

{{ Form::token() . Form::close() }}