<div id="header">
	<div class="container">
		<div id="welcomeLine" class="row">
			<div class="span12">
			@if(Auth::check())
			<div class="span5">
			Welcome!<strong> {{ Auth::user()->email }}</strong>
			</div>
				@if (Auth::user()->is_admin == 1)
			<div class="span4"><span class="btn btn-mini"><a href="{{ URL::to('admin') }}">Confidential</a></span></div>
				@endif
			@endif
			<div class="pull-right">
				<span class="btn btn-mini">€{{ Cart::total() }}</span>
				<a href="{{ URL::to('store/cart') }}"><span class="btn btn-mini btn-primary"><i class="icon-shopping-cart icon-white"></i> [ {{ Cart::totalItems() }} ] Items in your cart </span> </a> 
			</div>
			</div>
		</div>
		<!-- Navbar ================================================== -->
		<div id="logoArea" class="navbar">
		<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		  <div class="navbar-inner">
		    <a class="brand" href="{{ URL::to('/') }}">{{ HTML::image('images/logo.png', $alt="Avansshop", $attributes = array()) }}</a>
				{{ Form::open(array('url' => 'search', 'method'=>'get', 'class'=>'form-inline navbar-search', 'data-formID'=>'search')) }}
				<input placeholder="Search..." class="span3 form-control" id="srchFld" value="{{ (isset($term)) ? $term : Input::old('term') }}" name="term" type="text">
				{{ Form::select('category', $categories, $defaultSelection) }}
				<!-- <select>
					@foreach ($categories as $category=>$subcategory)
						@if (is_array($subcategory))
						<optgroup label="{{ $category }}">
							@foreach($subcategory as $category=>$subcategory)
								<option value="{{ $category }}">{{ $subcategory }}</option>
							@endforeach
						</optgroup>
						@else
						<option value="{{ $category }}">{{ $subcategory }}</option>
						@endif
					@endforeach
				</select> -->
				{{ Form::submit('Go', array('class' => 'btn btn-primary')) }}
				{{ Form::close() }}
		    <ul id="topMenu" class="nav pull-right">
		    <li class=""><a href="{{ URL::to('sessions/login') }}">Join</a></li>
			<li class=""><a href="{{ URL::to('contact') }}">Contact</a></li>
			 <li class="">
			 @if(Auth::check())
			 <a href="{{ URL::to('sessions/logout') }}" style="padding-right:0"><span class="btn btn-large btn-inverse">Logout</span></a>
			 @else
			 <a href="#login" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Login</span></a>
			 @endif
			<div id="login" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
				@include('layouts.partials.modals.login')
			</div>
			</li>
		    </ul>
		  </div>
		</div>
		<!--
		<div id="message-container">
		<?php
		if(isset($_SESSION['messages']) && $_SESSION['messages']){
			foreach($_SESSION['messages'] as $message){
				?>
				<div class="alert alert-<?= $message['type'] ?> alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <?php
		  echo $message['message'];
		  ?>
		</div>
		<?php
			}
			unset($_SESSION['messages']);
		}
		?>
		</div>
		-->
		@include('layouts.partials.errors.messages')
	</div>
</div>
<!-- Header End====================================================================== -->