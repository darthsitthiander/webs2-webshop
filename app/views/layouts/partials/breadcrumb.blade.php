<ul class="breadcrumb">
@foreach ($breadcrumb['items'] as $key => $breadcrumb_item)
	@if ($key == end($breadcrumb['array_keys']))
	<li class="active">
	@else
	<li>
	@endif

	@if ($breadcrumb_item['enabled'])
		<a href="<?= $breadcrumb_item['link'] ?>">
	@endif
			<?= $breadcrumb_item['name'] ?>

	@if ($breadcrumb_item['enabled'])
		</a>
	@endif

	@if ($key != end($breadcrumb['array_keys']))
		<span class="divider">/</span>
	@endif
	</li>
@endforeach
</ul>