<!-- Sidebar ================================================== -->
	<div id="sidebar" class="span3">
		<div class="well well-small">
			<a id="myCart" href="{{ URL::to('store/cart') }}">
				<img src="{{ asset('images/ico-cart.png') }}" alt="cart">{{ Cart::totalItems() }} Items in your cart  <span class="badge badge-warning pull-right">€{{ Cart::total() }}</span>
			</a>
		</div>
		<ul id="sideManu" class="nav nav-tabs nav-stacked">
			@foreach ($categories as $category=>$subcategories)
				@if (!is_array($subcategories))
					@if ($subcategories == "All Categories")
					<li class="subMenu"><a id="allcategories" href="{{ URL::to('categories') }}">All Categories</a></li>
					@endif
				@else
					<li class="subMenu open"><a>{{ $category }}</a>
						<ul>
					@foreach($subcategories as $subcategory)
							<li>
								<a href="{{ URL::to('categories/overview/' . $subcategory['id'] ) }}">
									<i class="icon-chevron-right"></i>{{ $subcategory['name'] }} ({{ $subcategory['number_of_products'] }})
								</a>
							</li>
					@endforeach
						</ul>
					</li>
				@endif
			@endforeach
		</ul>
		<br/>
		@foreach($products as $index=>$product)
			<div class="thumbnail">
				@if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
	            <a class="gallery" href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="{{ $product->name }}">
					<img src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" alt="{{ $product->name }}"/>
	            </a>
	            @else
	            <div style="padding: 20px 0 0 75px;">
				No Image available.
				</div>
	            @endif
	  
				<div class="caption">
				  <h5>{{ $product->name }}</h5>
					<h4 style="text-align:center">
						<a class="btn" href="{{ URL::to('product/details/' . $product->id) }}">
							<i class="icon-zoom-in"></i>
						</a>
						<!-- <a data-trigger="addToCart" data-id="1" data-quantity="1"> -->
							{{ Form::open(array('action' => 'StoreController@postAddtocart', 'style' => 'display: inline;', 'method'=>'post')) }}
			                {{ Form::hidden('quantity', 1) }}
			                {{ Form::hidden('id', $product->id) }}
			                <button type="submit" class="btn">
								Add to <i class="icon-shopping-cart"></i>
			                </button>
			                {{ Form::token() . Form::close() }}
						<!-- </a> -->
						<a class="btn btn-primary">€{{ $product->price_eur }}</a>
					</h4>
				</div>
			</div>
			@if ($index+1 != count($products))
			<br />
			@endif
		@endforeach
		<br />
		<div class="thumbnail">
			<img src="{{ asset('images/payment_methods.png') }}" title="Bootshop Payment Methods" alt="Payments Methods">
			<div class="caption">
			  <h5>Payment Methods</h5>
			</div>
		</div>
	</div>
<!-- Sidebar end=============================================== -->