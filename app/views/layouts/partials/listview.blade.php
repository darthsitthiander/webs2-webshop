	<div class="tab-pane" id="listView">
		@foreach ($products as $product)
		<div class="row">	  
			<div class="gallery span2" style="padding: 60px 0px 0px 0px;">
	            @if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
	            <a class="gallery" href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="{{ $product->name }}">
					<img src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" alt="{{ $product->name }}"/>
	            </a>
	            @else
	            <div style="padding: 20px;">
				No Image available.
				</div>
	            @endif
			</div>
  
			<div class="span4">
				<h3>New | Available</h3>				
				<hr class="soft">
				<h5>{{ $product['name'] }}</h5>
				<p>{{ $product['description'] }}</p>
				<div class="pull-right">
					{{ Form::open(array('action' => 'StoreController@postAddtocart', 'style' => 'display: inline;', 'method'=>'post')) }}
					<button type="submit" class="btn">
						Add to <i class="icon-shopping-cart"></i>
	                </button>
					<a href="{{ URL::to('product/details/' . $product['id']) }}" class="btn"><i class="icon-zoom-in"></i></a>
					{{ Form::hidden('quantity', 1) }}
			        {{ Form::hidden('id', $product->id) }}
					{{ Form::token() . Form::close() }}
				</div>
				<br class="clr">
			</div>
			<div class="span3 alignR">
				<form class="form-horizontal qtyFrm">
				<h3>€{{ $product['price_eur'] }}</h3>
				
				</form>
			</div>
		</div>
		<hr class="soft">
		@endforeach
	</div>