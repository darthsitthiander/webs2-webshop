@if($errors->any())
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	{{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif