@if(Session::has('message'))
	<div class="alert {{ Session::get('alert-class') }} alert-dismissable" style="margin: 15px;">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	{{ Session::get('message') }}
    </div>
@endif