<h3>Login</h3>

<div class="well">

{{ Form::open(array('route' => 'sessions.store', 'class' => 'form-horizontal loginFrm', 'data-target' => '#login', 'data-async' => 'true')) }}

@include('layouts.partials.errors.validation')


  <div class="control-group">								
	{{ Form::text('email', '', array('placeholder' => 'Email', 'id' => 'inputEmail')) }}
  </div>
  <div class="control-group">
	{{ Form::password('password', array('placeholder' => 'Password', 'id' => 'inputPassword', 'value' => Input::old('password'))) }}
  </div>
  <div class="control-group">
	<label class="checkbox">
	<input type="checkbox" name="remember_me"> Remember me
	</label>
  </div>


<div class="control-group">
	<div class="controls">
	  	<div class="btn-toolbar">
			{{ Form::submit('Login', array('class' => 'btn btn-success pull-left')) }}
			{{ Form::button('Close', array('class' => 'btn pull-right', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')) }}
			{{ HTML::link(URL::to('register'), 'Register', array('class' => 'btn btn-info pull-right', 'style' => 'line-height: 20px')) }}
	  	</div>
	</div>
</div>

{{ Form::token() . Form::close() }}

</div>