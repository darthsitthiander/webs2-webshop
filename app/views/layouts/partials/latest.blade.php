<!-- LATEST PRODUCTS -->
			<h4>Latest Products </h4>

			<ul class="thumbnails">

@foreach ($products as $key => $product)
	@if ($key % 3 == 0)
			</ul>
			<ul class="thumbnails">
	@endif
				<li class="span3">
				  <div class="thumbnail">
					<a href="{{ URL::to('product/details') . '/' . $product->id }}">

<?php
			// if (isset(Helpers::getImagePath($product->id, 'thumbnail')[0]) && file_exists(Helpers::getImagePath($product->id, 'thumbnail')[0])){
?>
@if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
<a class="gallery" href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="{{ $product->name }}">
	<img src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" alt="{{ $product->name }}"/>
</a>
@else
<div style="padding-top: 20px;">
No Image available.
</div>
@endif
					</a>
					<div class="caption">
					  <h5><?=$product->name?></h5>
					  <p> 
						<?=$product->description?>
					  </p>
					 
					  <h4 style="text-align:center">
					  	<a class="btn" href="{{ URL::to('product/details') . '/' . $product->id }}"><i class="icon-zoom-in"></i></a>
					  	{{ Form::open(array('action' => 'StoreController@postAddtocart', 'style' => 'display: inline;', 'method'=>'post')) }}
		                {{ Form::hidden('quantity', 1) }}
		                {{ Form::hidden('id', $product->id) }}
		                <button type="submit" class="btn">
							Add to <i class="icon-shopping-cart"></i>
		                </button>
		                {{ Form::token() . Form::close() }}
					  	<a class="btn btn-primary" href="#">€<?=$product->price_eur?></a>
					  </h4>
					</div>
				  </div>
				</li>
@endforeach
			</ul>