<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <title>Avanshop</title>
	    <base href="{{ URL::to('/') }}" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <!--Less styles -->
	   <!-- Other Less css file //different less files has different color scheam
		<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
		<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
		<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
		-->
		<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
		<script src="js/less.js" type="text/javascript"></script> -->
		
	<!-- Bootstrap style --> 
		{{ HTML::style('css/bootshop/bootstrap.min.css', array('id' => 'callCss', 'media' => 'screen')) }}
	<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> -->
		{{ HTML::style('css/base.css', array('media' => 'screen')) }}
	<!-- Bootstrap style responsive -->	
		<!--<link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>-->
		{{ HTML::style('css/font-awesome.css') }}
	<!-- Google-code-prettify -->	
		{{ HTML::style('js/google-code-prettify/prettify.css') }}
	<!-- fav and touch icons -->
	    <link rel="shortcut icon" href="{{ asset('images/ico/favicon.ico') }}">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png') }}">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png') }}">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png') }}">
	    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png') }}">
		<style type="text/css" id="enject"></style>

		{{ HTML::style('css/master.css') }}
	</head>
	<body>

		<div id="container">
			@include('layouts.partials.header')

			@if (Request::is('/'))
				@include('layouts.partials.carousel')
			@endif

			<div id="mainBody">
				<div class="container">
					<div class="row">
						@include('layouts.partials.sidebar')
						<div class="span9">
							@if (!Request::is('/'))
								@include('layouts.partials.breadcrumb')
							@endif
							@yield('content')
						</div>

					</div>
				</div>
			</div><!-- MainBody End ============================= -->
			@include('layouts.partials.footer')
		</div>
		<!-- Placed at the end of the document so the pages load faster ============================================= -->
	   	
	   	<!-- jQuery 1.11.0 -->
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

		{{ HTML::script('js/google-code-prettify/prettify.js') }}
		
		{{ HTML::script('js/bootshop.js') }}
		{{ HTML::script('js/jquery.lightbox-0.5.js') }}
		{{ HTML::script('js/master.js') }}

  	</body>
</html>