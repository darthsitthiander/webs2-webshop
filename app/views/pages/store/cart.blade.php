@extends('layouts.default')

@section('content')

	<h3>  SHOPPING CART [ <small>{{ Cart::totalItems() .' ' . ((Cart::totalItems() == 1) ? 'Item' : 'Items') }} </small>]<a href="{{ URL::previous() }}" class="btn btn-large pull-right"><i class="icon-arrow-left"></i> Continue Shopping </a></h3>	
	<hr class="soft"/>

	<table class="table table-bordered">
      <thead>
        <tr>
          <th>Product</th>
          <th>Description</th>
          <th>Quantity/Update</th>
		      <th>Price</th>
          <th>Tax</th>
          <th>Total</th>
		</tr>
      </thead>
      <tbody>
@foreach ($products as $product)
                <tr>
                  <td>
            			@if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
            			<div id ="gallery" class="span2" style="padding: 7px 0px 0px 0px; margin-right: -125px;">
            	            <a href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="<?=$product->name?>">
            					      <img width="60" src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" alt="<?=$product->name?>"/>
            	            </a>
            			</div>
            			@else
            			<div class="span2" style="padding: 7px 0px 0px 0px; margin-right: -75px;">
            			No Image available.
            			</div>
            			@endif
                  </td>
                  <td><a href="{{ URL::to('product/details/' . $product->id) }}"><?=$product->name?></a></td>
        				  <td>
        					<div class="input-append">
        						<input class="span1" style="max-width:34px" placeholder="{{ $product->quantity }}" id="appendedInputButtons" size="16" type="text">

                    <a href="{{ URL::to('/store/decrementitemincart/' . $product->identifier) }}" class="btn">
                      <i class="icon-minus"></i>
                    </a>

                    <a href="{{ URL::to('/store/incrementitemincart/' . $product->identifier) }}" class="btn">
                      <i class="icon-plus"></i>
                    </a>

        						<a href="{{ URL::to('/store/removeitem/' . $product->identifier) }}" class="btn btn-danger">
                      <i class="icon-remove icon-white"></i>
                    </a>
                  </div>
        				  </td>
                  <td>€{{ $product->price }}</td>
                  <td>{{ $product->tax }}%</td>
                  <td>€{{ number_format(Cart::total(),2) }}</td>
                </tr>
@endforeach
                <tr>
                  <td colspan="5" style="text-align:right">Subtotal</td>
                  <td>€{{ number_format($subtotal, 2) }}</td>
                </tr>
				        <tr>
                  <td colspan="5" style="text-align:right"><strong>TOTAL (with taxes)</strong></td>
                  <td class="label label-important" style="display:block"> <strong>€<?=Cart::total()?></strong></td>
                </tr>
				</tbody>
            </table>


	<a href="{{ URL::previous() }}" class="btn btn-large"><i class="icon-arrow-left"></i> Continue Shopping </a>
@if (Cart::totalItems() > 0)
	<a href="{{ URL::to('store/checkout') }}" class="btn btn-large pull-right">Check-out <i class="icon-arrow-right"></i></a>
@endif

@stop