@extends('layouts.default')

@section('content')
<h3>All Categories</h3>
<hr class="soft">
<p>Does this even need a description?</p>
<hr class="soft">
<div class="tab-pane  active" id="blockView">
	<ul class="thumbnails">
		@foreach($categories as $category)
		<li class="span3">
			<a href="{{ URL::to('categories/subcategories').'/'.$category['id'] }}">
			     <div class="thumbnail">
	    	    	<!-- <img style="display: none;" src="" /> -->
			    	<div class="caption">
			    	  <h5><?=$category['name']?></h5>
			    	  <p> 
			    		<?=$category['description']?>
			    	  </p>
			    	</div>
			    </div>
			</a>
		</li>
		@endforeach
	</ul>
</div>
@stop