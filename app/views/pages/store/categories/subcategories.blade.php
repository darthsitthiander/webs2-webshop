@extends('layouts.default')

@section('content')
<h3>{{ $category['name'] }}</h3>
<hr class="soft">
<p>{{ $category['description'] }}</p>
<hr class="soft">
<div class="tab-pane  active" id="blockView">
	<ul class="thumbnails">
		@foreach($subcategories as $category)
		<li class="span3">
		  <div class="thumbnail">
			<a href="{{ URL::to('categories/overview/'.$category['id']) }}">
				<div class="caption">
				  <h5><?=$category['name']?></h5>
				  <p> 
					<?=$category['description']?>
				  </p>
				</div>
			</a>
		  </div>
		</li>
		@endforeach
	</ul>
</div>
@stop