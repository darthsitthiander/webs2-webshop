@extends('layouts.default')

@section('content')
<h3>Checkout</h3>	
<hr class="soft"/>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><h5>Your order</h5></div>

    <!-- Table -->
    <table class="table">
      <thead>
        <tr>
          <th>Product name</th>
          <th>Price</th>
          <th>Quantity</th>
        </tr>
      </thead>
      <tbody>
@foreach ($products as $product)
        <tr>
          <td><?=$product->name?></td>
          <td><?=$product->price_eur?></td>
          <td><?=$product->quantity?></td>
        </tr>
@endforeach
      </tbody>
    </table>
  </div>

  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><h5>User credentials</h5></div>

    <div class="well">
      <table style="width: 100%;">
        <tr style=" border-bottom: 1px solid #DDD">
          <td style="width: 120px;"><strong>First name</strong></td>
          <td><?=$user['first_name']?></td>
        </tr>
        <tr style=" border-bottom: 1px solid #DDD">
          <td><strong>Last name</strong></td>
          <td><?=$user['last_name']?></td>
        </tr>
        <tr style=" border-bottom: 1px solid #DDD">
          <td><strong>Address</strong></td>
          <td><p><?=$user['address']?></p>
          <p><?=$user['city']?></p></td>
        </tr>
      </table>
    </div>

    <div class="btn-toolbar pull-right" role="toolbar">
      <div class="btn-group">
        <span class="btn btn-large">€<?=number_format(Cart::total(),2)?></span>
      </div>
      <div class="btn-group">
        <a href="{{ URL::to('/store/destroycart') }}" class="btn btn-large btn-primary">
          Confirm <i class="icon-arrow-right"></i>
        </a>
      </div>
    </div>

  </div>
@stop