@extends('layouts.default')

@section('content')
	<h3>Search<small class="pull-right">{{ count($products) }} products are available </small>
	</h3>
	<hr class="soft"/>
	<section id="search-keyword">
		Search results for <strong>{{ $term }}</strong>
	</section>
	<hr class="soft"/>

@if(count($products) == 0)
There are no products in this category.
@else
	<form class="form-horizontal span6">
		<div class="control-group">
		  <label class="control-label alignL">Sort By </label>
			<select>
              <option>Product name A - Z</option>
              <option>Product name Z - A</option>
              <option>Product Stoke</option>
              <option>Price Lowest first</option>
            </select>
		</div>
	  </form>
	  
<div id="myTab" class="pull-right">
 <a href="#listView" data-toggle="tab"><span class="btn btn-large"><i class="icon-list"></i></span></a>
 <a href="#blockView" data-toggle="tab"><span class="btn btn-large btn-primary"><i class="icon-th-large"></i></span></a>
</div>
<br class="clr"/>
<div class="tab-content">

@include('layouts.partials.listview')
@include('layouts.partials.blockview')

</div>
	<br class="clr"/>
@endif
@stop