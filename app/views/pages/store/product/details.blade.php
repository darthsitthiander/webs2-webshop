@extends('layouts.default')

@section('content')

<h3>Product Details</h3>
<div class="row">	  
	@if (isset($imagePaths[$product->id]['normal']) || isset($imagePaths[$product->id]['thumbnail']))
	<div id="gallery" class="span3" style="max-width: 230px;">
		<a style="vertical-align: middle;" href="{{ (isset($imagePaths[$product->id]['normal']) && asset($imagePaths[$product->id]['normal'])) ? asset($imagePaths[$product->id]['normal']) : asset($imagePaths[$product->id]['thumbnail']) }}" title="<?=$product['name']?>">
			<img style="display: block; margin-left: auto; margin-right: auto;" src="{{ (isset($imagePaths[$product->id]['thumbnail']) && asset($imagePaths[$product->id]['thumbnail'])) ? asset($imagePaths[$product->id]['thumbnail']) : asset($imagePaths[$product->id]['normal']) }}" title="<?=$product['name']?>"/>
	    </a>
	</div>
	@else
	<div class="gallery span3" style="margin: 100px -65px 0 0; padding-left: 65px">
	No Image available.
	</div>
	@endif
		<div class="span6">
			<h3><?= $product->name ?></h3>
			<small><?= $product->description ?></small>
			<hr class="soft"/>
			  <div class="control-group">
				<label class="control-label"><span>€ <?= $product->price_eur ?></span></label>
				<div class="controls">

					{{ Form::open(array('action' => 'StoreController@postAddtocart', 'style' => 'display: inline;', 'method'=>'post', 'class' => 'form-horizontal qtyFrm')) }}
					Quantity: <input type="number" min="1" name="quantity" class="span1" placeholder="1" value="1"/>
	                {{ Form::hidden('id', $product->id) }}
	                <button type="submit" class="btn btn-large btn-primary pull-right">
						Add to <i class="icon-shopping-cart"></i>
	                </button>
	                {{ Form::token() . Form::close() }}
				</div>
			  </div>
			
			<hr class="soft"/>
			<h4><?=$product['stock']?> items in stock</h4>
			<hr class="soft clr"/>
			<p><?= $product['long_description'] ?></p>
			<br class="clr"/>
		<a href="#" name="detail"></a>
		<hr class="soft"/>
		</div>
</div>

@stop