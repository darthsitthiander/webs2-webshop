@extends('layouts.default')

@section('content')

<h3>Visit us</h3>	
<div class="row">
	<div class="span4">
	<h4>Contact Details</h4>
	<p>	42 Musterstrasse,<br/> 5000, Tilburg
		<br/><br/>
		info@avanshop.nl<br/>
		﻿Tel 123-456-6780<br/>
		Fax 123-456-5679<br/>
	</p>		
	</div>
		
	<div class="span4">
	<h4>Opening Hours</h4>
		<h5> Monday - Friday</h5>
		<p>09:00am - 09:00pm<br/><br/></p>
		<h5>Saturday</h5>
		<p>09:00am - 07:00pm<br/><br/></p>
		<h5>Sunday</h5>
		<p>12:30pm - 06:00pm<br/><br/></p>
	</div>
	<div class="span4">
		<h4>Email Us</h4>
		<p style="color: #aa0000">This functionality has been temporarily disabled.</p>
		<form data-trigger="contactMail" class="form-horizontal">
        <fieldset>
          <div class="control-group">
           
              <input type="text" placeholder="name" class="input-xlarge"/>
           
          </div>
		   <div class="control-group">
           
              <input type="text" placeholder="email" class="input-xlarge"/>
           
          </div>
		   <div class="control-group">
           
              <input type="text" placeholder="subject" class="input-xlarge"/>
          
          </div>
          <div class="control-group">
              <textarea rows="3" id="textarea" class="input-xlarge"></textarea>
           
          </div>

            <button class="btn btn-large disabled" type="submit">Send Messages</button>

        </fieldset>
      	</form>
	</div>
	<div class="span4">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2480.509552804707!2d5.091382699999993!3d51.558891699999975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6bfccc630086b%3A0xc34da02e0225cd95!2sHeuvelring+92C!5e0!3m2!1sde!2sus!4v1396375316141" width="400" height="300" frameborder="0" style="border:0"></iframe>
	</div>
</div>

@stop