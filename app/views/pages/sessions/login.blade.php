@extends('layouts.default')

@section('content')

	<h3>Login</h3>	
	<hr class="soft"/>
	@if (!Auth::check())
		<div class="row">
			<div class="span4">
				<div class="well">
				<h5>ALREADY REGISTERED?</h5>

				{{ Form::open(array('url' => 'sessions/login', 'method'=>'POST', 'data-formID' => 'login-join', 'data-target' => '#login', 'data-async' => 'true')) }}
				
				@include('layouts.partials.errors.validation')

				  <div class="control-group">
					<label class="control-label" for="inputEmail1">Email</label>
					<div class="controls">
					{{ Form::text('email', '', array('placeholder' => 'Email', 'id' => 'inputEmail1', 'class' => 'span3', 'value' => Input::old('password'))) }}
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="inputPassword1">Password</label>
					<div class="controls">
					{{ Form::password('password', array('placeholder' => 'Password', 'id' => 'inputPassword', 'class' => 'span3', 'value' => Input::old('password'))) }}
					</div>
				  </div>
				  <div class="control-group">
					<label class="checkbox">
					<input type="checkbox" name="remember_me"> Remember me
					</label>
				  </div>
				  <div class="control-group">
					<div class="controls">
					  {{ Form::submit('Login', array('class' => 'btn')) }}
					  <!-- <a href="#">Forget password?</a> -->
					</div>
				  </div>
				{{ Form::hidden('quantity', Request::old('quantity')) }}
				{{ Form::hidden('id', Request::old('id')) }}
				{{ Form::hidden('url.intended', Session::get('url.intended')) }}
				{{ Form::token() . Form::close() }}

			</div>
			</div>
			<div class="span1"> &nbsp;</div>
			<div class="span4">
				<div class="well">
				<h5>CREATE YOUR ACCOUNT</h5><br/>
				Enter your e-mail address to create an account.<br/><br/><br/>
				{{ Form::open(array('url' => 'sessions/register', 'method' => 'GET', 'data-formID' => 'register-join')) }}
				   <div class="control-group">
					<label class="control-label" for="inputEmail0">E-mail address</label>
					<div class="controls">
					  <input class="span3" name="email" type="text" id="inputEmail0" placeholder="Email">
					</div>
				  </div>
				  <div class="controls">
				  <button type="submit" class="btn block">Create Your Account</button>
				  </div>
				{{ Form::token() . Form::close() }}
			</div>
			</div>
		</div>	
	@else
	You are already logged in.
	@endif

@stop