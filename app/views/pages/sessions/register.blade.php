@extends('layouts.default')

@section('content')

	<h3>Registration</h3>
	<hr class="soft"/>
	<div id="register-messages"></div>
	<div class="well">
	
	@include('layouts.partials.errors.validation')
	
	{{ Form::open(array('url'=>'sessions/register', 'method'=>'POST', 'data-formID' => 'register', 'class' => 'form-horizontal')) }}

		<h4>Your personal information</h4>
		<div class="control-group">
			<label class="control-label" for="inputFname1">First name <sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('fname', '', array('placeholder' => 'First Name', 'id' => 'inputFname1', 'value' => Input::old('fname'))) }}<br />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputLnam">Last name <sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('lname', '', array('placeholder' => 'Last Name', 'id' => 'inputLnam', 'value' => Input::old('lname'))) }}<br />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="input_email">Email <sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('email', '', array('placeholder' => 'Email', 'id' => 'input_email', 'value' => Input::old('email'))) }}<br />
			</div>
		</div>	  
		<div class="control-group">
			<label class="control-label" for="inputPassword1">Password <sup>*</sup></label>
			<div class="controls">
			{{ Form::password('password', array('placeholder' => 'Password', 'id' => 'inputPassword1', 'value' => Input::old('password'))) }}<br />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword1">Password confirmation <sup>*</sup></label>
			<div class="controls">
			{{ Form::password('password_confirmation', array('placeholder' => 'Password', 'id' => 'inputPassword1', 'value' => Input::old('password_confirmation'))) }}<br />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Date of Birth <sup>*</sup></label>
			<div class="controls">
			  <select class="span1" name="dDay">
					<option value="">-</option>
					<?php
					for ($i = 1; $i <= 31; $i++) {
					?>
						<option value="<?=$i?>" {{ (Input::old('dDay') == $i) ? 'selected' : '' }}><?=$i?></option>
					<?php
					}
					?>
				</select>
				<select class="span1" name="dMonth">
					<option value="">-</option>
					<?php
					for ($i = 1; $i <= 12; $i++) {
					?>
						<option value="<?=$i?>" {{ (Input::old('dMonth') == $i) ? 'selected' : '' }}><?=$i?></option>
					<?php
					}
					?>
				</select>
				<select class="span1" name="dYear">
					<option value="">-</option>
					<?php
					for ($i = date("Y"); $i >= 1900; $i--) {
					?>
						<option value="<?=$i?>" {{ (Input::old('dYear') == $i) ? 'selected' : '' }}><?=$i?></option>
					<?php
					}
					?>
				</select>
				<span>(DD/MM/YYYY)</span>
			</div>
		</div>
		<h4>Your address</h4>
		
		<div class="control-group">
			<label class="control-label" for="address">Address<sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('address', '', array('placeholder' => 'Adress', 'id' => 'address', 'value' => Input::old('address'))) }} <span>Street address, P.O. box, company name, c/o</span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="city">City<sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('city', '', array('placeholder' => 'City', 'id' => 'city', 'value' => Input::old('city'))) }}<br />
			</div>
		</div>	
		<div class="control-group">
			<label class="control-label" for="zip_code">Zip / Postal Code<sup>*</sup></label>
			<div class="controls">
			  {{ Form::text('zip_code', '', array('placeholder' => 'Zip / Postal Code', 'id' => 'postcode', 'value' => Input::old('zip_code'))) }}<br />
			</div>
		</div>
		
	<p><font color="red"><sup>*</sup>Required field	</font></p>
	
	<div class="control-group">
			<div class="controls">
				<div class="btn-toolbar">
					{{ Form::submit('Register', array('class' => 'btn btn-large btn-success')) }}
					{{ HTML::link('/', 'Cancel', array('class' => 'btn btn-danger pull-right')) }}
					{{ HTML::link('sessions/login', 'Login', array('class' => 'btn btn-primary pull-right')) }}
				</div>
			</div>
		</div>		
	{{ Form::token() . Form::close() }}
</div>

@stop