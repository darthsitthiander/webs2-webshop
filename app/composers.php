<?php

// TERMS AND CONDITIONS
View::composer('pages.store.tac', function($view)
{
	Breadcrumb::setPage('Terms & Conditions');
});

// FREQUENTLY ASKED QUESTIONS
View::composer('pages.store.faq', function($view)
{
	Breadcrumb::setPage('Frequently Asked Questions');
});

// Cart
View::composer('pages.store.cart', function($view)
{
	Breadcrumb::setPage('Shopping Cart');

	$products = Cart::contents();
	$subtotal = 0;

	$imagePaths = array();
	foreach ($products as $product) {
		$subtotal += $product->price;
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];
	}

	$view->with(array(
		'products' => $products,
		'subtotal' => $subtotal,
		'imagePaths' => $imagePaths
	));
});

// Checkout
View::composer('pages.store.checkout', function($view)
{
	Breadcrumb::setPage('Checkout');
	$products = Cart::contents();

	$view->with(array(
		'products' => $products,
		'user' => Auth::user()
	));
});

// Product Details
View::composer('pages.store.product.details', function($view)
{
	$id = Route::getCurrentRoute()->getParameter('id');
	$product = Product::find($id);
	Breadcrumb::setPage($product->name);
	Breadcrumb::addItem('All Categories', URL::to('categories'));

	$imagePaths = array();
	if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	if ($product->fk_category)
	{
		$category = Category::find($product->fk_category);
		$parent_category = Category::where('id', '=', $category->parent_category)->first();

		Breadcrumb::addItem($parent_category->name, URL::to('categories/subcategories/'.$parent_category->id));
		Breadcrumb::addItem($category->name, URL::to('categories/overview/'.$category->id));
		
	} else {
		Breadcrumb::addItem('Unknown', '', false);
		Breadcrumb::addItem('Unknown', '', false);
	}

	$view->with(array(
		'product' => $product,
		'imagePaths' => $imagePaths
	));
});

// Category Pages
View::composer('pages.store.categories.all', function($view)
{
	Breadcrumb::setPage('All Categories');

	$view->with(array(
		'categories' => Category::where('parent_category', '=', NULL)->get()
	)); 
});
View::composer('pages.store.categories.subcategories', function($view)
{
	$id = Route::getCurrentRoute()->getParameter('id');
	$category = Category::find($id);

	Breadcrumb::setPage($category->name);
	Breadcrumb::addItem('All Categories', URL::to('categories'));

	$subcategories = Category::where('parent_category', '=', $id)->get();

	$view->with(array(
		'subcategories' => $subcategories,
		'category' => $category
	));
});
View::composer('pages.store.categories.overview', function($view)
{
	$id = Route::getCurrentRoute()->getParameter('id');
	$category = Category::find($id);

	$parent_category = Category::where('id', '=', $category->parent_category)->first();

	Breadcrumb::setPage($category->name);
	Breadcrumb::addItem('All Categories', URL::to('categories'));
	Breadcrumb::addItem($parent_category->name, URL::to('categories/subcategories/'.$parent_category->id));

	$products = Category::find($id)->products()->paginate(6);

	$imagePaths = array();
	foreach($products as $product)
	{
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	}

	$view->with(array(
		'products' => $products,
		'category' => $category,
		'imagePaths' => $imagePaths
	));
});

// CONTACT
View::composer('pages.store.contact', function($view)
{
	Breadcrumb::setPage('Contact');
});

// SEARCH
View::composer('pages.store.search', function($view)
{
	Breadcrumb::setPage('Search');

	$term = Input::get('term');

	$category = Input::get('category');
	$products = ($category == "all") 
				? Product::where('name', 'LIKE', '%'.$term.'%')->paginate(6)
				: Product::where('name', 'LIKE', '%'.$term.'%')->where('fk_category', '=', $category)->paginate(6);
				
	$imagePaths = array();
	foreach($products as $product)
	{
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	}

	$view->with(array(
		'products' => $products,
		'imagePaths' => $imagePaths,
		'term' => $term
	));
});

// Header
View::composer('layouts.partials.header', function($view)
{
  	$defaultSelection = Input::get('category');
	$results = DB::select( DB::raw("SELECT c.id, c.name, c.description, c2.id AS parent_id, c2.name AS parent_name, count(p.id) AS number_of_products
		FROM products p
		RIGHT JOIN categories c ON c.id = p.fk_category
		LEFT JOIN categories c2 ON c2.id = c.parent_category
		GROUP BY c.id
		ORDER BY c2.id ASC") );
	$categories = array('all'=>'All Categories');
	foreach ($results as $category) {
		if($category->parent_id != NULL){
    		$categories[$category->parent_name][$category->id] = $category->name;
		} else {
			$categories[$category->name] = [];
		}
	}

	$view->with(array(
		'categories' => $categories,
		'defaultSelection' => $defaultSelection
	));
});

// Sidebar & Featured
View::composer('layouts.partials.sidebar', function($view)
{
	$results = DB::select( DB::raw("SELECT c.id, c.name, c.description, c2.id AS parent_id, c2.name AS parent_name, count(p.id) AS number_of_products
		FROM products p
		RIGHT JOIN categories c ON c.id = p.fk_category
		LEFT JOIN categories c2 ON c2.id = c.parent_category
		GROUP BY c.id
		ORDER BY c2.id ASC") );

	$categories = array('all'=>'All Categories');
	foreach ($results as $category) {
		if($category->parent_id != NULL){
    		$categories[$category->parent_name][$category->id] = array('id'=>$category->id, 'name'=>$category->name, 'number_of_products'=>$category->number_of_products);
		} else {
			$categories[$category->name] = [];
		}
	}

	$products = Product::where('featured_sidebar', '=', '1')->take(16)->orderBy('created_at', 'DESC')->get();

	$imagePaths = array();
	foreach($products as $product)
	{
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	}

	$view->with(array(
		'categories' => $categories,
		'products' => $products,
		'imagePaths' => $imagePaths
	));
});

// Featured
View::composer('layouts.partials.featured', function($view)
{
	$products = Product::where('featured', '=', '1')->take(16)->orderBy('created_at', 'DESC')->get();

	$imagePaths = array();
	foreach($products as $product)
	{
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	}
	
	$view->with(array(
		'products' => $products,
		'imagePaths' => $imagePaths
	));
});

// Latest
View::composer('layouts.partials.latest', function($view)
{

	$products = Product::take(6)->orderBy('created_at', 'DESC')->get();

	$imagePaths = array();
	foreach($products as $product)
	{
		if(Helpers::getImagePaths($product->id)) $imagePaths[$product->id] = Helpers::getImagePaths($product->id)[0];

	}

	$view->with(array(
		'products' => $products,
		'imagePaths' => $imagePaths
	));
});

// Breadcrumb
View::composer('layouts.partials.breadcrumb', function($view)
{
	$breadcrumb['items'] = Breadcrumb::getItems();
	$breadcrumb['array_keys'] = array_keys($breadcrumb['items']);

	$view->with(array(
		'breadcrumb' => $breadcrumb
	));
});

// REGISTER
View::composer('pages.sessions.register', function($view)
{
	Breadcrumb::setPage('Register');
});

// LOGIN
View::composer('pages.sessions.login', function($view)
{
	Breadcrumb::setPage('Login');
});