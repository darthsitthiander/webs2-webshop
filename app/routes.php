<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@showHome']);

Route::get('contact', 'HomeController@showContact');
Route::get('tac', 'HomeController@showTac');
Route::get('faq', 'HomeController@showFaq');
Route::get('search', 'HomeController@getSearch');

Route::group(array('prefix' => 'categories'), function()
{

	Route::get('/', 'HomeController@showCategoriesAll');
	Route::get('/overview/{id}', 'HomeController@showCategoriesOverview');
	Route::get('/subcategories/{id}', 'HomeController@showCategoriesSubcategories');

});
Route::get('user/{id}/profile', array('before' => 'auth', 'uses' => 'HomeController@showProfile'));

Route::get('sessions/logout', 'SessionController@destroy');
Route::get('sessions/login', 'HomeController@showLogin');
Route::get('sessions/register', 'HomeController@showRegister');
Route::post('sessions/login', 'SessionController@postLogin');
Route::post('sessions/register', 'SessionController@postRegister');

Route::get('language/{lang}', 
   array(
          'as' => 'language.select', 
          'uses' => 'LanguageController@select'
    )
);

Route::get('product/details/{id}', 'HomeController@showProductDetails');

Route::controller('store', 'StoreController');

// Route::get('store/addtocart', 'StoreController@postAddtocart');
