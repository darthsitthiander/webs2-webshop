<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->decimal('price_eur', 10,2);
			$table->decimal('tax', 10,2);
			$table->text('long_description');
			$table->integer('fk_category');
			$table->tinyInteger('featured');
			$table->tinyInteger('featured_sidebar');
			$table->integer('stock');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
