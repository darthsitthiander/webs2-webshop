<?php

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$electronics = new Category;
		$electronics->name = 'Electronics';
		$electronics->description = "description";
		$electronics->save();		

		$mobilePhones = new Category;
		$mobilePhones->name = 'Mobile phones';
		$mobilePhones->parent_category = $electronics->id;
		$mobilePhones->description = "description";
		$mobilePhones->save();

		$household = new Category;
		$household->name = 'Household';
		$household->description = "description";
		$household->save();

		$knives = new Category;
		$knives->name = 'Knives';
		$knives->parent_category = $household->id;
		$knives->description = "description";
		$knives->save();

		$miscellaneous = new Category;
		$miscellaneous->name = 'Miscellaneous';
		$miscellaneous->description = "description";
		$miscellaneous->save();

		$miscellaneous_sub = new Category;
		$miscellaneous_sub->name = 'Miscellaneous';
		$miscellaneous_sub->description = "description";
		$miscellaneous_sub->parent_category = $miscellaneous->id;
		$miscellaneous_sub->save();
	}

}