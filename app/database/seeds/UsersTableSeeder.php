<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$user = new User;
		$user->email = 'admin@webs2webshop.com';
		$user->first_name = 'Admin';
		$user->last_name = 'Istrator';
		$user->address = 'Musterstrasse 38';
		$user->city = 'Musterhausen';
		$user->zip_code = '4242XM';
		$user->date_of_birth = '1942-01-01';
		$user->is_admin = 1;
		$user->password = Hash::make('1234567');
		$user->save();
	}

}