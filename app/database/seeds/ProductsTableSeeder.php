<?php

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$nexus = new Product;
		$nexus->name = 'Nexus 5 16GB';
		$nexus->description = "Black";
		$nexus->fk_category = '2';
		$nexus->featured = 1;
		$nexus->featured_sidebar = 1;
		$nexus->save();

		$knife = new Product;
		$knife->name = 'Knife';
		$knife->description = "Dangerous";
		$knife->fk_category = '4';
		$knife->featured = 1;
		$knife->save();

		$peterpan = new Product;
		$peterpan->name = 'Peter Pan';
		$peterpan->description = "Magically flying around abducting young children into fantasy worlds";
		$peterpan->fk_category = '6';
		$peterpan->featured = 1;
		$peterpan->save();
		
		$faker = Faker\Factory::create();

		//Product::truncate();

		foreach(range(1,30) as $index)
		{
			Product::create([
				'name' => $faker->sentence,
				'description' => $faker->paragraph(4),
				'long_description' => $faker->paragraph(4),
				'fk_category' => 6
			]);
		}
	}
}