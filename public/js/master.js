jQuery(function($) {
    $("form[data-formID='register']").on('submit', function(event) {
        //event.preventDefault();

        var $form = $(this);

        if (!checkRegister($form.serializeArray())) {
            return false;
        }

    });

    function checkRegister(data) {
        var fieldname = "";
        var error = true;
        var message = "";
        var tmpMessage = "";

        $(data).each(function(x) {

            var value = data[x]["value"];
            
            switch (data[x]["name"]) {
                case "fname":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "first name" : "";
                    break; 
                case "lname":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "last name" : "";
                    break; 
                case "email":
                    fieldname = (value == "" || !isValidEmailAddress(value)) ? "email" : "";
                    break; 
                case "password":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "password" : "";
                    break; 
                case "dDay":
                    fieldname = (value == "" || !isNumeric(value)) ? "date of birh (DD)" : "";
                    break; 
                case "dMonth":
                    fieldname = (value == "" || !isNumeric(value)) ? "date of birh (MM)" : "";
                    break; 
                case "dYear":
                    fieldname = (value == "" || !isNumeric(value)) ? "date of birh (YYYY)" : "";
                    break; 
                case "address":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "address" : "";
                    break; 
                case "city":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "city" : "";
                    break; 
                case "postcode":
                    fieldname = (value == "" || !isAlphanumeric(value)) ? "zip code" : "";
            }

            if (fieldname != "") {
                error = false;

                tmpMessage += '<li class="error">Please fill in the required field for \''+fieldname+'\' correctly.</li>';
            }
            if(!error)
            {
                message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + tmpMessage + '</div>';
            }
        });
        $("#register-messages").html(message);
        return error;
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        // alert( pattern.test(emailAddress) );
        return pattern.test(emailAddress);
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function isAlphanumeric(s) {
        return s.match(/^[a-zA-Z0-9]+/);
    }
});