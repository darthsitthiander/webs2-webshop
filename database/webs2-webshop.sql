-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 28. Apr 2014 um 21:17
-- Server Version: 5.6.16
-- PHP-Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `webs2-webshop`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_category` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_category`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', NULL, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(2, 'Mobile phones', 1, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(3, 'Household', NULL, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(4, 'Knives', 3, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(5, 'Miscellaneous', NULL, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(6, 'Miscellaneous', 5, 'description', '2014-04-25 23:04:02', '2014-04-25 23:04:02');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_04_17_153019_create_users_table', 1),
('2014_04_18_234034_create_products_table', 1),
('2014_04_19_125308_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_eur` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `featured_sidebar` tinyint(4) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price_eur`, `tax`, `long_description`, `fk_category`, `featured`, `featured_sidebar`, `stock`, `created_at`, `updated_at`) VALUES
(1, 'Nexus 5 16GB', 'Black', '0.00', '0.00', '', 2, 1, 1, 0, '2014-04-25 23:04:01', '2014-04-25 23:04:01'),
(2, 'Knife', 'Dangerous', '0.00', '0.00', '', 4, 1, 0, 0, '2014-04-25 23:04:01', '2014-04-25 23:04:01'),
(3, 'Peter Pan', 'Magically flying around abducting young children into fantasy worlds', '0.00', '0.00', '', 6, 1, 0, 0, '2014-04-25 23:04:01', '2014-04-25 23:04:01'),
(4, 'Corrupti quo deserunt blanditiis corporis.', 'Consectetur esse cumque nulla voluptatem. Repellendus est iste saepe atque. Et suscipit deleniti alias accusantium recusandae. Sed quia commodi error omnis quis et et.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:01', '2014-04-25 23:04:01'),
(5, 'Eum dolorum nesciunt quo sit provident sequi ut.', 'Eum dolores magnam officia non nulla. Aut architecto voluptatem voluptas facilis et. Aliquam fugiat est maiores voluptas culpa. Aperiam quo eos explicabo illum aut voluptatem et.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(6, 'Ut minus beatae et quo.', 'Et dolorem et sed inventore. Sequi et dolor cum a nemo expedita ut. Sapiente tenetur eos inventore veritatis.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(7, 'Asperiores quidem facilis ut assumenda officia alias modi.', 'Tenetur vel sint earum sint nesciunt. Autem et asperiores dolores nesciunt sapiente molestias. Ut rem odit omnis autem in ratione libero perspiciatis. Et hic harum omnis et facilis incidunt.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(8, 'Fugit perspiciatis nostrum est maiores.', 'Nobis consequuntur nulla facere. Tempore unde qui corporis similique enim a. Autem sint impedit et esse aperiam voluptate. Ut sit vel fugit qui nihil odit qui. Est nesciunt ipsam et laudantium est officia quia.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(9, 'Illum laborum ipsam error ut distinctio voluptatem tempora tempore.', 'Magnam et quos occaecati ipsam. Ut illo enim illum numquam. Animi dolorem sunt itaque nihil nam perspiciatis neque. Aspernatur perferendis magni necessitatibus. Reprehenderit sequi inventore fuga perferendis ad.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(10, 'Ut nihil aliquid suscipit assumenda.', 'Voluptas beatae amet est sed incidunt ut. Deserunt ipsum quibusdam repellendus doloremque occaecati. Necessitatibus laudantium debitis inventore deleniti. Doloribus in qui deleniti et vitae aliquam qui illum. Laborum nulla officiis rerum et.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(11, 'Quo dolore maxime qui quo architecto facere tenetur.', 'Voluptatem omnis architecto voluptatum et similique blanditiis non doloremque. Aspernatur ullam incidunt quaerat corrupti amet quia cupiditate voluptas. Quisquam dolor enim quisquam est reprehenderit dolor mollitia deleniti. Aut perspiciatis voluptates in', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(12, 'Ducimus unde unde sit qui.', 'Quibusdam laudantium ea debitis consequatur eum. Explicabo hic quo facere doloribus id et fuga. Autem eveniet qui delectus aliquam quia.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(13, 'Sunt culpa est enim.', 'Rem sed blanditiis id enim. Necessitatibus doloribus et aliquid dolore. Sunt repellat repellendus vel esse vero accusantium. Ad quae est earum ea repellendus.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(14, 'Aliquid velit eos maxime assumenda ut ratione fuga.', 'Et debitis molestiae odit quia temporibus architecto. Dolorem qui omnis accusantium aut. Adipisci minima ut reiciendis iusto debitis aut.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(15, 'Rerum perspiciatis impedit harum quaerat quis.', 'Eveniet consectetur dolore dolores in. Officia animi tenetur consequuntur deserunt perferendis. Iste vel vel earum impedit sed cupiditate. Ipsa ullam quas ut odit nam.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(16, 'Consequatur vel iusto voluptates modi debitis.', 'Illum dolorum nulla esse incidunt ipsum. Nisi error omnis ut consequatur ut asperiores dolore earum. Unde aut aliquid voluptatibus atque commodi qui asperiores. Aperiam culpa veritatis possimus perspiciatis ut et aliquid. Excepturi officia beatae accusamu', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(17, 'Ex mollitia distinctio pariatur ab.', 'Autem facere voluptas cupiditate rerum delectus sit aut. A numquam ut alias amet deleniti harum vero. Quis porro nostrum voluptates et voluptatem at accusantium. Quas quia sed aut qui nesciunt.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(18, 'Vitae voluptatem veniam maiores error explicabo quaerat.', 'Quis hic dicta qui consequatur voluptas nulla. Molestias non est veritatis sit vel. Qui excepturi accusantium sint eos veritatis et vero ratione. Cumque sit minima harum nulla.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(19, 'Illo impedit unde fuga ut quibusdam adipisci laudantium saepe.', 'Assumenda ut tempore exercitationem. Commodi incidunt optio cupiditate quis. Quidem et rerum quis atque iure voluptatem ut sit.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(20, 'Voluptate adipisci reiciendis esse quibusdam et.', 'Et ex est odit sit ipsum. Quo distinctio quis quas sit tempore ut. Ea ullam quasi et atque. Vero assumenda laborum soluta perspiciatis. Tenetur ea voluptate dolor magnam inventore est.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(21, 'Ex ut veritatis quaerat at.', 'Officiis aut ducimus sapiente. Est commodi voluptatem amet voluptatem vel aut sed. Esse aut molestiae qui ex molestias labore corporis.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(22, 'Est ex est dolorem dolor dolorem aut reprehenderit optio.', 'Repudiandae et aut fugiat quia. Minus eius numquam iusto optio. Deleniti ut aut modi quo. Perferendis excepturi voluptatem excepturi molestiae.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(23, 'Suscipit quod maiores deserunt odit.', 'Quisquam rerum iusto est rerum. Qui sequi et quo quidem commodi suscipit. Quod cum voluptatem earum saepe. Vitae omnis rerum quod nostrum sed voluptas fugiat. Rerum nam et sint neque voluptate doloribus ut. Dolor aspernatur soluta id nesciunt vel esse.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(24, 'Aut qui ipsam sed similique.', 'Veritatis distinctio vel id nemo. Rem eos et ut in et magnam impedit. Dolorem exercitationem est aut ut exercitationem fuga blanditiis. Iusto voluptas perferendis in aspernatur. Saepe voluptas expedita nesciunt rerum enim ratione.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(25, 'Aliquam qui est quo amet.', 'Dolorem architecto qui qui voluptatem recusandae excepturi. Nulla itaque sapiente quibusdam qui. Sapiente architecto dignissimos dolor eos earum nemo. Tempore possimus dolorem eos sit et.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(26, 'Velit nostrum quasi repellendus quod.', 'Excepturi ad quae placeat rerum. Earum pariatur sit ratione voluptatum qui. Eaque nulla voluptas minima accusantium est doloremque qui. Laudantium esse eligendi tempore eos enim ad quia. Aut omnis dolores vitae et aliquid ut ut. Est eos aut corrupti iste ', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(27, 'At enim rerum asperiores quo explicabo.', 'Vitae a ea error voluptates dignissimos voluptas excepturi enim. Perferendis dolor ut necessitatibus recusandae eos odit quisquam quam. Veniam earum et aut molestias recusandae ut. Error dignissimos omnis dolor quod.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(28, 'Quidem aut odit aut cumque maxime rem ratione.', 'Et quia est sunt itaque. Temporibus et dolorum minus magnam perferendis quod natus eos. Nisi ipsam pariatur aut aspernatur. Molestias rem dolores neque sunt incidunt qui.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(29, 'Incidunt illum officiis sed modi eius quam quisquam.', 'Perferendis voluptatem et hic sint voluptatem sit. Eligendi porro eos aliquid exercitationem. Aut ut commodi dolore quidem sit modi. Qui quasi veniam alias velit quae voluptatem iusto ut.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(30, 'Necessitatibus est ad vitae iste quis architecto.', 'Quia rerum nesciunt eligendi. Incidunt quod qui in architecto sed ea. Eveniet hic magni repudiandae quis dolores qui labore. Dolores aut hic dolorem. Sapiente voluptate deleniti natus quo.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(31, 'Ut officia dolor at voluptatem aut eligendi.', 'Sequi laudantium fuga dolore explicabo. Consectetur aut facere deserunt. Rerum consequatur fuga dolor quod. Commodi deleniti et numquam non labore qui neque. Excepturi corrupti sit sapiente quas quo perspiciatis qui. Repellat non et ea nulla odio.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(32, 'Necessitatibus consequatur qui dolorum quod sed.', 'Quo quibusdam veritatis iusto. Deserunt consequatur qui temporibus voluptate. Omnis inventore id illum id cum aliquid. Rem quidem error repellat repudiandae quisquam ex.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02'),
(33, 'Ut voluptatem doloribus rerum.', 'Occaecati omnis commodi corporis sed fugit. Quos qui odit error maiores. Voluptatem nihil ducimus sit consequatur qui nobis. Ut quas dolorem suscipit magni commodi.', '0.00', '0.00', '', 6, 0, 0, 0, '2014-04-25 23:04:02', '2014-04-25 23:04:02');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `address`, `city`, `zip_code`, `date_of_birth`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Istrator', 'admin@webs2webshop.com', '$2y$10$h7ZJyuzpV8Q/uIVZ4Gd7FeAzyclwcattH.ZLH8jx6qFeQfpStaZUS', 'Musterstrasse 38', 'Musterhausen', '4242XM', '1942-01-01', 1, 'uWuCBpmGPcEbaQUoYTeTaNzbLH3mZgWeihzv3GcQDG4Oo6tqqJRo5L06y55o', '2014-04-25 23:04:01', '2014-04-28 09:50:55'),
(8, 'Sander', 'Schnydrig', 'sanderschnydrig@gmail.com', '$2y$10$p3pqCoOsYu8HsbhSCHFzkOaYzVDp0Bu9XFbcBWkELg5VgXezZ2lBC', 'Hazelaarlaan 11', 'Berkel-Enschot', '5056XM', '1991-06-13', 0, 'nfvx9B9l9neyIUS5NgXHkqgDCcedw0jfMlHdvYXV8aEEQsX4XTzWDLoou1mS', '2014-04-28 11:25:22', '2014-04-28 16:28:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
